const INITIAL_TIME_LEFT = 25
const MINUTE = 1
const NO_TIME_LEFT = 0

class Pomodoro {
  constructor(bus) {
    this.bus = bus
    this.timeLeft = NO_TIME_LEFT

    this._subscribe()
  }

  _subscribe() {
    this.bus.subscribe('timer.start_requested', this.start.bind(this))
    this.bus.subscribe('timer.askTimeLeft', this.calculateTimeLeft.bind(this))
    this.bus.subscribe('timer.pause', this.pause.bind(this))
    this.bus.subscribe('timer.reset', this.reset.bind(this))
  }

  start() {
    if(!this._hasTimeLeft()) { this._initializeTimeLeft() }

    this.bus.publish('timer.start', this._message())
  }

  pause() {
  }

  reset() {
  }

  calculateTimeLeft() {
    if(this._hasTimeLeft()) { this.timeLeft -= MINUTE }

    this.bus.publish('timer.timeLeft', this._message())
  }

  _initializeTimeLeft() {
    this.timeLeft = INITIAL_TIME_LEFT
  }

  _hasTimeLeft() {
    return (this.timeLeft > NO_TIME_LEFT)
  }
  _message() {
    return { minutes: this.timeLeft }
  }
}

export default Pomodoro
