const DISABLED_START_BUTTON = '<button id="start" disabled>Start</button>'
const START_BUTTON = '<button id="start">Start</button>'
const PAUSE_BUTTON = '<button id="pause">Pause</button>'
const DISABLED_PAUSE_BUTTON = '<button id="pause" disabled>Pause</button>'
const RESET_BUTTON = '<button id="reset">Reset</button>'

const NO_TIME_LEFT = 0

class Timer {
  constructor(document) {
    this.document = document
  }

  render(properties) {
    const minutes = properties.minutes

    return `
      <div id="container">
        <div id="buttons">
          ${this._startButton(properties)}
          ${this._pauseButton()}
          ${this._resetButton()}
        </div>
        <div id="timer">
          <div id="time">
            <span id="minutes">${this._format(minutes)}</span>
          </div>
          <div id="filler"></div>
        </div>
      </div>
    `
  }

  

  addCallbacks(callbacks) {
    this._addOnClickToStart(callbacks.startCountDown)
  }

  _format(number) {
    let result = number.toString()

    if (this._thereIsADigit(number)) { result = `0${result}` }

    return result
  }

  _startButton(countdown) {
    if (this._isRunning(countdown)) { return DISABLED_START_BUTTON }

    return START_BUTTON
  }

  _pauseButton() {
    if (this._isPaused()) { return DISABLED_PAUSE_BUTTON }
    return PAUSE_BUTTON
  }

  _resetButton() {
    
    return RESET_BUTTON
  }

  _addOnClickToStart(callback) {
    this._addOnClickTo('#start', callback)
  }

  _addOnClickToPause(callback) {
    this._addOnClickTo('#pause', callback)
  }

  _addOnClickToReset(callback) {
    this._addOnClickTo('#reset', callback)
  }

  _addOnClickTo(id, callback) {
    const element = this.document.querySelector(id)
    element.onclick = callback
  }

  _isRunning({minutes}) {
    return (minutes > NO_TIME_LEFT)
  }

  _isPaused(){
return
  }

  _isStarted(countdown) {
    return (countdown.minutes > NO_TIME_LEFT)
  }

  _thereIsADigit(number) {
    return (number < 10)
  }
}

export default Timer
