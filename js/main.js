import PomodoroComponent from './components/Pomodoro.js'
import PomodoroService from './services/Pomodoro.js'
import Bus from './libraries/Bus.js';

const bus = new Bus()

const container = document.querySelector('#pomodoro-app')
const pomodoro = new PomodoroComponent(container, bus, document)
pomodoro.draw()

new PomodoroService(bus)
